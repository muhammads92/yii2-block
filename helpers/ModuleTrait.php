<?php

namespace muhammads92\block\helpers;

/**
 * @property \muhammads92\block\Module $module
 */
trait ModuleTrait
{
    /**
     * @var null|\muhammads92\block\Module
     */
    private $_module;

    /**
     * @return null|\muhammads92\block\Module
     */
    protected function getModule()
    {
        if ($this->_module == null) {
            $this->_module = \Yii::$app->getModule('block');
        }

        return $this->_module;
    }
}