<?php

use yii\helpers\Html;
use muhammads92\block\widgets\BlockAsset;

/**
 * @var yii\web\View $this
 * @var muhammads92\block\models\Block $model
 */
?>
<?php BlockAsset::register($this);?>

<?php if (Yii::$app->user->isGuest): ?>

    <?= $model->value ?>

<?php else: ?>
    <div class="inline-block-admin" style="display: <?= $display ?>;">
        <div class="button-wrapper">
            <!--<div style="position: absolute;width: 100%;">-->
                    <?=
                    Html::a(Yii::t('block', 'Edit'), ['/admin/block/admin/update', 'id' => $model->id, 'mode' => 'back'], [
                        'class' => 'btn btn-success btn-inline-block-edit',
                        'onclick' => 'blockAddReturnUrl(this);',
                        'style' => 'z-index:1000;',
                    ])
                    ?>
            <!--</div>-->
            
        </div>
        <?= $model->value ?>
    </div>
<?php endif ?>