<?php

namespace muhammads92\block\widgets;

use yii\web\AssetBundle;

/**
* Block asset bundle
*
* @author Muhammad Samadov <sam.muhammadali@gmail.com>
*/
class BlockAsset extends AssetBundle
{
    public $sourcePath = '@vendor/muhammads92/yii2-block/widgets/views/block/assets';
    public $css = [
        'block.css',
    ];
    public $js = [
        'block.js',
    ];
}