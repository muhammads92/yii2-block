HTML blocks
===========
Yii2 module for HTML blocks editing

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist muhammads92/yii2-block "*"
```

or add

```
"muhammads92/yii2-block": "*"
```

to the require section of your `composer.json` file.

After running 

```
php composer.phar update
```

run

```
yii migrate --migrationPath=@vendor/muhammads92/yii2-block/migrations
```

After that change your main configuration file ```config/web.php```

```php
<?php return [
    ...
    'modules' => [
        ...
        'block' => [
            'class' => 'muhammads92\block\Module',
        ],
        ...
    ],
    ...
];
```

Unzip ajexFileManager.zip inner to root folder (its for basic-app)


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \muhammads92\block\widgets\Block::widget(['code' => 'about']); ?>
```